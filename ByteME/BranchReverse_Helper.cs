﻿using System;
using System.Collections.Generic;
using System.Text;
namespace ByteME
{
    class BranchReverse_Helper
    {
        public BranchReverse_Helper()
        {
            AddBranchs("2c", "2d");
            AddBranchs("2f", "31");
            AddBranchs("30", "32");
            AddBranchs("34", "36");
            AddBranchs("39", "3a");
            AddBranchs("35", "37");
            AddBranchsA("33", "36");
            AddBranchsA("40", "43");
            AddBranchs("3c", "3e");
            AddBranchs("3d", "3f");
            AddBranchs("41", "43");
            AddBranchs("42", "44");
        }
        static Dictionary<string, string> exchange = new Dictionary<string, string>();
        static void AddBranchs(string a, string b)
        {
            exchange.Add(a, b);
            exchange.Add(b, a);
        }
        static void AddBranchsA(string a, string b)
        {
            exchange.Add(a, b);
        }
        public bool Reverse(ref string value)
        {
            string newValue = string.Empty;
            bool exchanged = exchange.TryGetValue(value, out newValue);
            if (exchanged) value = newValue;
            return exchanged;
        }

    }
}
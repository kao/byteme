﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace ByteME.GUI
{
    public partial class errorHandlerForm : Form
    {
        string asmname;
        string error;
        string stacktrace;

        public errorHandlerForm(string asmname, string error, string stacktrace)
        {
            this.asmname = asmname;
            this.error = error;
            this.stacktrace = stacktrace;
            InitializeComponent();
            errorNameTextbox.Text = error;
            stackTraceTextBox.Text = stacktrace;
        }

        private void continueButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void errorHandlerForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                base.Close();
            }
        }

        private void sendReportButton_Click(object sender, EventArgs e)
        {
            try
            {
                // add your own reporting :P
            }
            catch
            {
                this.Close();
            }
        }

    }
}

﻿namespace ByteME.GUI
{
    partial class errorHandlerForm
    {
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ignoreAllErrorsCheckBox = new System.Windows.Forms.CheckBox();
            this.sendReportButton = new System.Windows.Forms.Button();
            this.continueButton = new System.Windows.Forms.Button();
            this.stackTraceTextBox = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.errorNameTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ignoreAllErrorsCheckBox
            // 
            this.ignoreAllErrorsCheckBox.AutoSize = true;
            this.ignoreAllErrorsCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ignoreAllErrorsCheckBox.Location = new System.Drawing.Point(9, 230);
            this.ignoreAllErrorsCheckBox.Name = "ignoreAllErrorsCheckBox";
            this.ignoreAllErrorsCheckBox.Size = new System.Drawing.Size(112, 19);
            this.ignoreAllErrorsCheckBox.TabIndex = 13;
            this.ignoreAllErrorsCheckBox.Text = "Ignore all errors";
            this.ignoreAllErrorsCheckBox.UseVisualStyleBackColor = true;
            // 
            // sendReportButton
            // 
            this.sendReportButton.Location = new System.Drawing.Point(138, 228);
            this.sendReportButton.Name = "sendReportButton";
            this.sendReportButton.Size = new System.Drawing.Size(113, 23);
            this.sendReportButton.TabIndex = 12;
            this.sendReportButton.Text = "Send report";
            this.sendReportButton.UseVisualStyleBackColor = true;
            this.sendReportButton.Click += new System.EventHandler(this.sendReportButton_Click);
            // 
            // continueButton
            // 
            this.continueButton.Location = new System.Drawing.Point(261, 228);
            this.continueButton.Name = "continueButton";
            this.continueButton.Size = new System.Drawing.Size(110, 23);
            this.continueButton.TabIndex = 11;
            this.continueButton.Text = "Continue";
            this.continueButton.UseVisualStyleBackColor = true;
            this.continueButton.Click += new System.EventHandler(this.continueButton_Click);
            this.continueButton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.errorHandlerForm_KeyPress);
            // 
            // stackTraceTextBox
            // 
            this.stackTraceTextBox.Location = new System.Drawing.Point(8, 44);
            this.stackTraceTextBox.Name = "stackTraceTextBox";
            this.stackTraceTextBox.ReadOnly = true;
            this.stackTraceTextBox.Size = new System.Drawing.Size(363, 178);
            this.stackTraceTextBox.TabIndex = 10;
            this.stackTraceTextBox.Text = "";
            this.stackTraceTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.errorHandlerForm_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Stack trace :";
            // 
            // errorNameTextbox
            // 
            this.errorNameTextbox.Location = new System.Drawing.Point(46, 5);
            this.errorNameTextbox.Name = "errorNameTextbox";
            this.errorNameTextbox.ReadOnly = true;
            this.errorNameTextbox.Size = new System.Drawing.Size(325, 20);
            this.errorNameTextbox.TabIndex = 8;
            this.errorNameTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.errorHandlerForm_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Error :";
            // 
            // errorHandlerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 259);
            this.Controls.Add(this.ignoreAllErrorsCheckBox);
            this.Controls.Add(this.sendReportButton);
            this.Controls.Add(this.continueButton);
            this.Controls.Add(this.stackTraceTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.errorNameTextbox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "errorHandlerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Error Reporting";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.errorHandlerForm_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.CheckBox ignoreAllErrorsCheckBox;
        private System.Windows.Forms.Button sendReportButton;
        private System.Windows.Forms.Button continueButton;
        private System.Windows.Forms.RichTextBox stackTraceTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox errorNameTextbox;
        private System.Windows.Forms.Label label1;
    }
}
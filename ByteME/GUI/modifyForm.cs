﻿using dnlib.DotNet.Emit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ByteME.GUI
{
    public partial class modifyForm : Form
    {
        public delegate void writed(uint rva, string bytes);
        public writed write;
        public delegate void refreshd(object o, EventArgs e);
        public refreshd refresh;
        string bytes = string.Empty;
        uint va;

        public modifyForm(string bytes, uint va)
        {
            InitializeComponent();
            this.va = va;

            if (IL_Helper.Hex2IL(bytes) != bytes)
            {
                opCodeRadioButton.Enabled = opCodeRadioButton.Checked = true;
                
                var values = Enum.GetValues(typeof(Code));

                foreach (Code oC in values)
                {
                    object opcode = oC.ToString().ToLower().Replace('_', '.');
                    opCodeListCmboBox.Items.Add(opcode);
                    if (oC.ToString().ToLower().Replace('_', '.') == IL_Helper.Hex2IL(bytes) )
                    {
                        opCodeListCmboBox.SelectedItem=opcode;
                    }
                }
                
            }

            this.textBox1.Text = bytes;
            this.textBox1.MaxLength = textBox1.Text.Length;
            this.bytes = bytes;
        }

        private void opCodeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Visible = !opCodeRadioButton.Checked;
            opCodeListCmboBox.Visible = opCodeRadioButton.Checked;
            
            if (textBox1.Visible)
            {
                textBox1.Text = IL_Helper.IL2Hex(opCodeListCmboBox.Text);
            }
        }

        private void modifyButton_Click(object sender, EventArgs e)
        {
            if (opCodeRadioButton.Checked)
                write(va, IL_Helper.IL2Hex(opCodeListCmboBox.Text));
            else
                write(va, textBox1.Text);

            //refresh(null, null);
            this.Close();
        }

    }
}

﻿namespace ByteME.GUI
{
    partial class modifyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.opCodeRadioButton = new System.Windows.Forms.RadioButton();
            this.bytesRadioButton = new System.Windows.Forms.RadioButton();
            this.fillWithNopButton = new System.Windows.Forms.Button();
            this.modifyButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.opCodeListCmboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // opCodeRadioButton
            // 
            this.opCodeRadioButton.AutoSize = true;
            this.opCodeRadioButton.Enabled = false;
            this.opCodeRadioButton.Location = new System.Drawing.Point(91, 32);
            this.opCodeRadioButton.Name = "opCodeRadioButton";
            this.opCodeRadioButton.Size = new System.Drawing.Size(79, 17);
            this.opCodeRadioButton.TabIndex = 14;
            this.opCodeRadioButton.Text = "As OpCode";
            this.opCodeRadioButton.UseVisualStyleBackColor = true;
            this.opCodeRadioButton.CheckedChanged += new System.EventHandler(this.opCodeRadioButton_CheckedChanged);
            // 
            // bytesRadioButton
            // 
            this.bytesRadioButton.AutoSize = true;
            this.bytesRadioButton.Checked = true;
            this.bytesRadioButton.Location = new System.Drawing.Point(9, 32);
            this.bytesRadioButton.Name = "bytesRadioButton";
            this.bytesRadioButton.Size = new System.Drawing.Size(65, 17);
            this.bytesRadioButton.TabIndex = 13;
            this.bytesRadioButton.TabStop = true;
            this.bytesRadioButton.Text = "As bytes";
            this.bytesRadioButton.UseVisualStyleBackColor = true;
            // 
            // fillWithNopButton
            // 
            this.fillWithNopButton.Location = new System.Drawing.Point(225, 7);
            this.fillWithNopButton.Name = "fillWithNopButton";
            this.fillWithNopButton.Size = new System.Drawing.Size(75, 22);
            this.fillWithNopButton.TabIndex = 12;
            this.fillWithNopButton.Text = "fill with nop";
            this.fillWithNopButton.UseVisualStyleBackColor = true;
            // 
            // modifyButton
            // 
            this.modifyButton.Location = new System.Drawing.Point(311, 7);
            this.modifyButton.Name = "modifyButton";
            this.modifyButton.Size = new System.Drawing.Size(75, 22);
            this.modifyButton.TabIndex = 11;
            this.modifyButton.Text = "modify";
            this.modifyButton.UseVisualStyleBackColor = true;
            this.modifyButton.Click += new System.EventHandler(this.modifyButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(9, 9);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(208, 20);
            this.textBox1.TabIndex = 10;
            // 
            // opCodeListCmboBox
            // 
            this.opCodeListCmboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.opCodeListCmboBox.FormattingEnabled = true;
            this.opCodeListCmboBox.Location = new System.Drawing.Point(9, 7);
            this.opCodeListCmboBox.Name = "opCodeListCmboBox";
            this.opCodeListCmboBox.Size = new System.Drawing.Size(208, 21);
            this.opCodeListCmboBox.TabIndex = 15;
            this.opCodeListCmboBox.Visible = false;
            // 
            // modifyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 57);
            this.Controls.Add(this.opCodeListCmboBox);
            this.Controls.Add(this.opCodeRadioButton);
            this.Controls.Add(this.bytesRadioButton);
            this.Controls.Add(this.fillWithNopButton);
            this.Controls.Add(this.modifyButton);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "modifyForm";
            this.Text = "modify";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton opCodeRadioButton;
        private System.Windows.Forms.RadioButton bytesRadioButton;
        private System.Windows.Forms.Button fillWithNopButton;
        private System.Windows.Forms.Button modifyButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox opCodeListCmboBox;
    }
}
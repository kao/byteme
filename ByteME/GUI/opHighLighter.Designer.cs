﻿namespace ByteME.GUI
{
    partial class opHighLighter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.removeButton = new System.Windows.Forms.Button();
            this.highLightedList = new System.Windows.Forms.ListBox();
            this.addButton = new System.Windows.Forms.Button();
            this.colorSelectorButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.opCodeListCmboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(234, 224);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(99, 25);
            this.removeButton.TabIndex = 20;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // highLightedList
            // 
            this.highLightedList.FormattingEnabled = true;
            this.highLightedList.Location = new System.Drawing.Point(15, 97);
            this.highLightedList.Name = "highLightedList";
            this.highLightedList.Size = new System.Drawing.Size(318, 121);
            this.highLightedList.TabIndex = 19;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(234, 63);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(99, 25);
            this.addButton.TabIndex = 18;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // colorSelectorButton
            // 
            this.colorSelectorButton.BackColor = System.Drawing.Color.Green;
            this.colorSelectorButton.Location = new System.Drawing.Point(79, 35);
            this.colorSelectorButton.Name = "colorSelectorButton";
            this.colorSelectorButton.Size = new System.Drawing.Size(254, 23);
            this.colorSelectorButton.TabIndex = 17;
            this.colorSelectorButton.UseVisualStyleBackColor = false;
            this.colorSelectorButton.Click += new System.EventHandler(this.colorSelectorButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Color :";
            // 
            // opCodeListCmboBox
            // 
            this.opCodeListCmboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.opCodeListCmboBox.FormattingEnabled = true;
            this.opCodeListCmboBox.Location = new System.Drawing.Point(79, 6);
            this.opCodeListCmboBox.Name = "opCodeListCmboBox";
            this.opCodeListCmboBox.Size = new System.Drawing.Size(254, 21);
            this.opCodeListCmboBox.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "OpCode:";
            // 
            // opHighLighter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 256);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.highLightedList);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.colorSelectorButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.opCodeListCmboBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "opHighLighter";
            this.Text = "OpCode HighLighter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.opHighLighter_FormClosing);
            this.Load += new System.EventHandler(this.opHighLighter_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.ListBox highLightedList;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button colorSelectorButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox opCodeListCmboBox;
        private System.Windows.Forms.Label label1;
    }
}
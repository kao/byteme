﻿using dnlib.DotNet.Emit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ByteME.GUI
{
    public partial class opHighLighter : Form
    {
        string list = string.Empty;

        public opHighLighter()
        {
            InitializeComponent();
            var values = Enum.GetValues(typeof(Code));
            foreach (Code oC in values)
            {
                opCodeListCmboBox.Items.Add(oC.ToString().ToLower().Replace('_', '.'));
            }
            opCodeListCmboBox.Sorted = true;
        }

        private void colorSelectorButton_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.CustomColors = new int[] { 0 };
            cd.ShowDialog();
            colorSelectorButton.BackColor = cd.Color;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (opCodeListCmboBox.SelectedIndex != -1)
            {
                highLightedList.Items.Add(opCodeListCmboBox.Text.Remove(opCodeListCmboBox.Text.Length - 1, 0) + "," + colorSelectorButton.BackColor.ToArgb().ToString());
                list += opCodeListCmboBox.Text.Remove(opCodeListCmboBox.Text.Length - 1, 0) + "," + colorSelectorButton.BackColor.ToArgb().ToString() + "\r\n";
            }
        }

        private void opHighLighter_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!System.IO.File.Exists(Application.StartupPath + "\\" + "highlighting"))
            {
                System.IO.File.WriteAllText(Application.StartupPath + "\\" + "highlighting", list);

            }
            else
            {
                System.IO.File.AppendAllText(Application.StartupPath + "\\" + "highlighting", list);
            }
        }

        private void opHighLighter_Load(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(Application.StartupPath + "\\" + "highlighting"))
            {
                foreach (string line in System.IO.File.ReadAllText(Application.StartupPath + "\\" + "highlighting").Split('\n'))
                {
                    if (line != string.Empty) { highLightedList.Items.Add(line); }
                }
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            try
            {
                string original = System.IO.File.ReadAllText(Application.StartupPath + "\\" + "highlighting");
                string replaced = original.Replace(highLightedList.SelectedItem.ToString(), "");
                System.IO.File.WriteAllText(Application.StartupPath + "\\" + "highlighting", replaced);
                highLightedList.Items.Remove(highLightedList.SelectedItem);
            }
            catch { }
        }

    }
}

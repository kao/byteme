﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using dnlib;
using dnlib.DotNet;
using dnlib.PE;
using dnlib.DotNet.MD;
using dnlib.DotNet.Emit;
using dnlib.IO;

namespace ByteME.GUI
{
    public partial class mainForm : Form
    {
        List<MemoryStream> assemblyStreams = new List<MemoryStream>();
        string assemblyLocation = string.Empty;
        BranchReverse_Helper BranchReverser = new BranchReverse_Helper();
        AssemblyDef assembly;
        int currentAssemblyIndex = 0;
        string highlightedOPCodes = string.Empty;
        bool resetSearch = false;

        public mainForm(string[] args)
        {
            InitializeComponent();

            try
            {
                this.Size = new System.Drawing.Size(
                Properties.Settings.Default.sizex, Properties.Settings.Default.sizey
                );
            }
            catch (Exception) { }
            if (File.Exists(Application.StartupPath + "\\highlighting"))
            {
                highlightedOPCodes = File.ReadAllText(Application.StartupPath + "\\highlighting");
            }

            searchListComboBox.SelectedIndex = 0;

            processFiles(args);
        }

        private void processFiles(string[] args)
        {
            foreach (string file in args)
            {
                FileInfo fileInfo = new FileInfo(file);
                if (!fileInfo.Exists)
                {
                    MessageBox.Show(file + " doesn't exist!");
                    continue;
                }

                try
                {
                    MemoryStream assemblyStream = new MemoryStream(File.ReadAllBytes(file));
                    TreeNode assemblyNode = processAssembly(AssemblyDef.Load(assemblyStream));
                    assemblyStreams.Add(assemblyStream);

                    assemblyNode.Tag = file;
                    deassemblyTreeView.Nodes.Add(assemblyNode);
                    deassemblyTreeView.SelectedNode = assemblyNode;
                }
                catch (BadImageFormatException)
                {
                    MessageBox.Show(fileInfo.Name + " - is not a valid dotnet assembly");
                }


            }
        }

        private TreeNode processAssembly(AssemblyDef assembly)
        {
            TreeNode assemblyNode = new TreeNode(assembly.Name);
            List<string> namespaces = new List<string>();

            foreach (TypeDef type in assembly.ManifestModule.Types)
            {
                TreeNode nameSpaceNode = null;
                string namespacekey = (type.Namespace.ToString() == null) ? "nullspace" : type.Namespace.ToString();
                if (!namespaces.Contains(type.Namespace))
                {
                    namespaces.Add(type.Namespace);
                    nameSpaceNode = new TreeNode(type.Namespace);
                    nameSpaceNode.Name = namespacekey;
                    assemblyNode.Nodes.Add(nameSpaceNode);
                }
                else
                {
                    nameSpaceNode = assemblyNode.Nodes.Find(namespacekey, false)[0];
                }
                nameSpaceNode.SelectedImageIndex = nameSpaceNode.ImageIndex = 1;

                try
                {
                    nameSpaceNode.Nodes.Add(processType(type));
                }
                catch { }

            }

            return assemblyNode;
           // deassemblyTreeView.Nodes.Add(assemblyNode);

        }

        private TreeNode processType(TypeDef type)
        {
            TreeNode typeNode = new TreeNode(type.Name);
            typeNode.Tag = type.MDToken.ToInt32();

            try
            {
                foreach (TypeDef nestedType in type.NestedTypes)
                {
                    typeNode.Nodes.Add(processType(nestedType));
                }
            }
            catch { }

            try
            {
                foreach (MethodDef method in type.Methods)
                {
                    TreeNode methodNode = new TreeNode(method.Name);
                    methodNode.Tag = method.MDToken.ToInt32();
                    methodNode.Text += "(";
                    int pIndex = 0;
                    foreach (Parameter param in method.Parameters)
                    {
                        methodNode.Text += param.Type.FullName.ToString();
                        if (++pIndex != method.Parameters.Count)
                            methodNode.Text += ",";
                    }
                    methodNode.Text += ")";
                    typeNode.Nodes.Add(methodNode);
                    methodNode.SelectedImageIndex = methodNode.ImageIndex = 3;
                }
            }
            catch { }

            typeNode.SelectedImageIndex = typeNode.ImageIndex = 2;

            return typeNode;
        }

        private void loadAssemblyInfo(AssemblyDef assembly)
        {
            infoListView.Items.Clear();

            infoListView.Items.Add("Version").SubItems.Add(assembly.Version.ToString());
            infoListView.Items.Add("Compiler version").SubItems.Add(assembly.ManifestModule.RuntimeVersion);
            //infoListView.Items.Add( "Obfuscator", Obfdetector_Helper.detectObfuscator(assembly, fs) });
            infoListView.Items.Add("Module kind").SubItems.Add(assembly.ManifestModule.Kind.ToString());
            
            string epToken = string.Empty;
            if (assembly.ManifestModule.EntryPoint != null)
            {
                epToken = assembly.ManifestModule.EntryPoint.MDToken.ToInt32().ToString("x");
            }
            infoListView.Items.Add("Entrypoint token").SubItems.Add(epToken);
      //      infoListView.Items.Add("Metadata section").SubItems.Add(assembly.ManifestModule.Image.MetadataSection.Name);

    //        DotNetFile file = DotNetFile.Load(assemblyStreams[0]);
        }

        private void deassembleMethod(MethodDef method, DotNetFile pe)
        {
            try
            {

                rvaLabel.Text = "Method RVA: " + method.RVA.ToString("x").ToUpper();
                tokenLabel.Text = "Method Token: " + method.MDToken.ToInt32().ToString("x").ToUpper();

                retTypeLabel.Text = "Return Type: " + method.ReturnType.FullName.ToString();
            }
            catch { }

            try
            {
                if (!method.HasBody) return;
            }
            catch (Exception) { return; }
            CilBody body;


            try
            {
                body = method.Body;

                uint headerSize = (method.Body.MaxStack != 8) ? 12u : 1u;

                uint methodVA = (uint)pe.MetaData.PEImage.ToFileOffset(method.RVA);
                    

                foreach (Instruction inc in body.Instructions)
                {
                    uint incRVA = (uint)(method.RVA + inc.Offset + headerSize);

                    FileOffset incVA = pe.MetaData.PEImage.ToFileOffset((RVA)incRVA);
                    ListViewItem instructionItem = new ListViewItem(inc.Offset.ToString("x").ToUpper());
                    instructionItem.SubItems.Add(incVA.ToString("x").ToUpper());
                    instructionItem.SubItems.Add(incRVA.ToString("x").ToUpper());
                    instructionItem.SubItems.Add(inc.OpCode.ToString());

                    int bytesLength = 4;
                    
                    seek(methodVA + headerSize + (uint)inc.Offset);

                    if (inc.Operand != null)
                    {
                        object operand = inc.Operand;
                        if (inc.OpCode == OpCodes.Switch)
                        {
                            try
                            {
                                operand = "";
                                foreach (Instruction sIncs in (dnlib.DotNet.Emit.Instruction[])inc.Operand)
                                {
                                    operand += sIncs.Operand.ToString();
                                }

                            }
                            catch { }
                        }
                        instructionItem.SubItems.Add(operand.ToString());

                        if (inc.OpCode.Size == 2)
                        {
                            bytesLength += 1;
                        }
                        else if (inc.OpCode.FlowControl == FlowControl.Branch || inc.OpCode.FlowControl == FlowControl.Cond_Branch || inc.OpCode.OpCodeType == OpCodeType.Macro)
                        {
                            bytesLength = 1;
                        }
                    }
                    else
                    {
                        instructionItem.SubItems.Add("");
                        bytesLength = 0;
                    }

                    string bytes = string.Empty;
                    for (int index = 0; index <= bytesLength; ++index)
                    {
                        string byt = assemblyStreams[currentAssemblyIndex].ReadByte().ToString("x");
                        if (byt.Length == 1)
                        {
                            byt = "0" + byt;
                        }
                        bytes += byt.ToUpper();
                    }

                    instructionItem.SubItems.Add(bytes);

                    highlight(instructionItem);

                    deassemblyListView.Items.Add(
                        instructionItem);
                }

            }
            catch (Exception e)
            {
                errorHandler.handleException(e, assembly.Name.ToString());
                //HandleException
                return;
            }
        }

        private void deassembleType(TypeDef type)
        {
            try
            {
                rvaLabel.Text = "Type Token: " + type.MDToken.ToInt32().ToString("x").ToUpper();
            }
            catch (Exception) { }
            tokenLabel.Text = string.Empty;
            retTypeLabel.Text = string.Empty;
        }

        private object tokenResolver(int mdtoken)
        {
            foreach(TypeDef type in assembly.ManifestModule.Types)
            {
                foreach (TypeDef nestedType in type.NestedTypes)
                    if (nestedType.MDToken.ToInt32() == mdtoken) return nestedType;

                foreach (MethodDef method in type.Methods)
                    if (method.MDToken.ToInt32() == mdtoken) return method;

                if (type.MDToken.ToInt32() == mdtoken) return type;
            }

            return null;
        }

        private void seek(uint address)
        {
            assemblyStreams[currentAssemblyIndex].Seek(address, SeekOrigin.Begin);
        }

        public void write(uint offset, string bytes)
        {
            byte[] bytes_ = IL_Helper.toByteArray(bytes);
            seek(offset);
            for (int i = 0; i < bytes_.Length; i++)
            {
                assemblyStreams[currentAssemblyIndex].WriteByte(bytes_[i]);
            }
        }

        private void highlight(ListViewItem inc)
        {
            string opCode = inc.SubItems[3].Text.ToLower();
            foreach (string highlight in highlightedOPCodes.Split('\n'))
            {
                if (highlight.ToLower().Split(',')[0] == opCode.ToLower())
                {
                    inc.BackColor =
                        System.Drawing.Color.FromArgb(int.Parse(highlight.Split(',')[1]));
                }
            }
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                processFiles(ofd.FileNames);
            }
        }

        private void deassemblyTreeView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void deassemblyTreeView_DragDrop(object sender, DragEventArgs e)
        {
            processFiles((string[])e.Data.GetData(DataFormats.FileDrop));
        }

        private void deassemblyTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode assemblyNode = deassemblyTreeView.SelectedNode; while (assemblyNode.Parent != null) { assemblyNode = assemblyNode.Parent; }
            if (assemblyNode.Tag.ToString() != assemblyLocation)
            {
                assemblyLocation = assemblyNode.Tag.ToString();
                this.Text = "ByteME - " + assemblyLocation;
                currentAssemblyIndex = assemblyNode.Index;
                this.assembly = AssemblyDef.Load(assemblyStreams[currentAssemblyIndex]);
                loadAssemblyInfo(this.assembly);
            }

            deassemblyListView.Visible = !(deassemblyTreeView.SelectedNode.Level == 0);

            switch (deassemblyTreeView.SelectedNode.ImageIndex)
            {
                case 3:
                    deassemblyListView.Items.Clear();
                    MethodDef method = (MethodDef)tokenResolver((int)deassemblyTreeView.SelectedNode.Tag);
                    deassembleMethod(method, DotNetFile.Load(assemblyLocation));
                    break;
                case 2:
                    TypeDef type = (TypeDef)tokenResolver((int)deassemblyTreeView.SelectedNode.Tag);
                    deassembleType(type);
                    break;
            }

        }

        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            Properties.Settings.Default.sizex = this.Size.Width;
            Properties.Settings.Default.sizey = this.Size.Height;

            Properties.Settings.Default.Save();

            Console.WriteLine(Properties.Settings.Default.sizex);
            if (assemblyStreams.Count != 0)
            {
                this.Hide();
                foreach (MemoryStream memStream in assemblyStreams)
                {
                    memStream.Dispose();
                    memStream.Close();
                }
            }

        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            if (assemblyStreams.Count != 0 && assemblyStreams[currentAssemblyIndex] != null)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.OverwritePrompt = false;
                sfd.InitialDirectory = Path.GetDirectoryName(assemblyLocation);
                sfd.FileName = new FileInfo(assemblyLocation).Name;

                sfd.Filter = "Exeuctable (*.exe)|*.exe|All files (*.*)|*.*";

                if (sfd.ShowDialog() == DialogResult.OK)
                {

                    using (Stream s = new FileStream(sfd.FileName, FileMode.Create, FileAccess.Write))
                    {
                        assemblyStreams[currentAssemblyIndex].WriteTo(s);
                    }
                    MessageBox.Show("successfully saved!", "",MessageBoxButtons.OK, MessageBoxIcon.Information);

                    assembly = AssemblyDef.Load(assemblyStreams[currentAssemblyIndex]);
                }
                

            }
        }

        private void closeToolStripButton_Click(object sender, EventArgs e)
        {
            if (assemblyStreams.Count != 0 && assemblyStreams[currentAssemblyIndex] != null)
            {
                this.Text = "ByteME";
                assemblyStreams[currentAssemblyIndex].Close();
                assemblyStreams.RemoveAt(currentAssemblyIndex);

                TreeNode TopNode = deassemblyTreeView.SelectedNode;
                while (TopNode.Parent != null) { TopNode = TopNode.Parent; }
                deassemblyTreeView.Nodes.Remove(TopNode);

                assemblyLocation = string.Empty;

                deassemblyListView.Items.Clear();
                infoListView.Items.Clear();

            }
        }

        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            if (deassemblyTreeView.SelectedNode != null)
            {
                assembly = AssemblyDef.Load(assemblyStreams[currentAssemblyIndex]);
                deassemblyTreeView_AfterSelect(null, null);
            }
        }

        private void opCodeHighlightingToolStripButton_Click(object sender, EventArgs e)
        {
            opHighLighter opHighLight = new opHighLighter();
            opHighLight.ShowDialog();

            highlightedOPCodes = File.ReadAllText(Application.StartupPath + "\\highlighting");
        }

        private void traceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (deassemblyListView.SelectedItems.Count != 0 && deassemblyListView.SelectedItems[0].SubItems[5].Text.EndsWith("6") &&
                deassemblyListView.SelectedItems[0].SubItems[5].Text.Length > 7)
            {
                int searchIndex = (deassemblyListView.SelectedItems[0].SubItems[5].Text.EndsWith("6")) ? 3 : 1;
                string revToken = IL_Helper.ReverseToken(deassemblyListView.SelectedItems[0].SubItems[5].Text.Remove(0, 2)).Remove(0, 1);
                search(revToken, searchIndex);
            }
        }

        private string getInstructionRVA(MethodDef method, int iOffset)
        {
            uint va = (uint)DotNetFile.Load(assemblyStreams[currentAssemblyIndex].ToArray()).MetaData.PEImage.ToFileOffset(method.RVA);
            seek(va + 1);
            int i = 1;
            int b = assemblyStreams[currentAssemblyIndex].ReadByte();
            if (b == 0x30)
            {
                i = 12;
            }
            return ( (uint)method.RVA + method.Body.Instructions[iOffset].Offset + i).ToString("x");
        }

        private void search(string pattern, int index)
        {
            TreeNode node = deassemblyTreeView.SelectedNode;
            while (node.Parent != null)
            {
                node = node.Parent;
            }
            if (index == 0)
            {
                foreach (TreeNode namespacenodes in node.Nodes)
                {
                    foreach (TreeNode typenodes in namespacenodes.Nodes)
                    {
                        if (typenodes.Text.ToLower().Contains(pattern.ToLower())
                            && (deassemblyTreeView.SelectedNode == null
                            || resetSearch == true
                            || deassemblyTreeView.SelectedNode.Parent == null
                            || (deassemblyTreeView.SelectedNode.Parent == namespacenodes && deassemblyTreeView.SelectedNode.Index < typenodes.Index)
                            || (deassemblyTreeView.SelectedNode.Parent != namespacenodes && deassemblyTreeView.SelectedNode.Parent.Index < namespacenodes.Index)
                            ))
                        {

                            resetSearch = false;
                            deassemblyTreeView.SelectedNode = typenodes;
                            goto escape;
                        }
                    }
                }
            }
            else if (index == 1 && pattern.Length == 7)
            {
                foreach (TreeNode namespacenodes in node.Nodes)
                {
                    foreach (TreeNode typenodes in namespacenodes.Nodes)
                    {
                        TypeDef typ = null;
                        try
                        {
                            typ =assembly.ManifestModule.FindNormal(namespacenodes.Text + typenodes.Text);
                        }
                        catch (ArgumentException)
                        {
                            typ = (TypeDef)tokenResolver((int)typenodes.Tag);// TokenToType(typenodes.Tag.ToString());
                        }
                        if (typ.MDToken.ToInt32().ToString("x").ToLower() == pattern.ToLower())
                        {
                            deassemblyTreeView.SelectedNode = typenodes;
                            goto escape;
                        }
                    }
                }
            }
            else if (index == 2)
            {
                foreach (TreeNode namespacenodes in node.Nodes)
                {
                    foreach (TreeNode typenodes in namespacenodes.Nodes)
                    {
                        foreach (TreeNode method in typenodes.Nodes)
                        {
                            if (method.Text.ToLower().Contains(pattern.ToLower())
                            && (deassemblyTreeView.SelectedNode == null
                            || resetSearch == true
                            || (deassemblyTreeView.SelectedNode.Level == 1 && namespacenodes == deassemblyTreeView.SelectedNode)
                            || (deassemblyTreeView.SelectedNode.Level == 2 && typenodes == deassemblyTreeView.SelectedNode)
                            || (deassemblyTreeView.SelectedNode.Level == 3 && typenodes == deassemblyTreeView.SelectedNode.Parent && deassemblyTreeView.SelectedNode.Index < method.Index)
                            || (deassemblyTreeView.SelectedNode.Level == 3 && typenodes != deassemblyTreeView.SelectedNode.Parent && namespacenodes.Index > deassemblyTreeView.SelectedNode.Parent.Parent.Index
                            || (deassemblyTreeView.SelectedNode.Level == 3 && typenodes != deassemblyTreeView.SelectedNode.Parent && namespacenodes == deassemblyTreeView.SelectedNode.Parent.Parent && typenodes.Index > deassemblyTreeView.SelectedNode.Parent.Index)
                            )))
                            {
                                resetSearch = false;
                                deassemblyTreeView.SelectedNode = method;
                                goto escape;
                            }

                        }
                    }
                }
            }
            else if ((index == 3 && pattern.Length == 7) || index == 4 || index == 5 || index == 6 || index == 7)
            {
                foreach (TreeNode namespacenodes in node.Nodes)
                {
                    foreach (TreeNode typenodes in namespacenodes.Nodes)
                    {
                        int methodindex = 0;
                        TypeDef type = null;

                        type = (TypeDef)tokenResolver((int)typenodes.Tag);

                        try
                        {
                            foreach (MethodDef method in type.Methods)
                            {
                                if (index == 3)
                                {
                                    if (method.MDToken.ToInt32().ToString("x").ToLower() == pattern.ToLower())
                                    {
                                        deassemblyTreeView.SelectedNode = typenodes.Nodes[methodindex + method.DeclaringType.NestedTypes.Count];
                                        goto escape;
                                    }
                                }
                                else if (index == 4)
                                {
                                    if (method.RVA.ToString("x").ToLower() == pattern.ToLower())
                                    {
                                        deassemblyTreeView.SelectedNode = typenodes.Nodes[methodindex + method.DeclaringType.NestedTypes.Count];
                                        goto escape;
                                    }
                                }
                                else if (index == 5 || index == 6 || index == 7)
                                {
                                    int incindex = 0;
                                    foreach (Instruction inc in method.Body.Instructions)
                                    {
                                        if (
                                           (inc.Operand != null && index == 5 && inc.Operand.ToString().ToLower().Contains(pattern.ToLower())
                                           || (inc.Operand != null && index == 6 && (inc.OpCode == OpCodes.Ldstr) && inc.Operand.ToString().ToLower().Contains(pattern.ToLower()))
                                           || (index == 7 && (getInstructionRVA(method, incindex).ToLower() == pattern.ToLower()))
                                           )

                                            && (deassemblyTreeView.SelectedNode == null
                                || resetSearch == true
                                || (deassemblyTreeView.SelectedNode.Level == 1 && namespacenodes == deassemblyTreeView.SelectedNode)
                                || (deassemblyTreeView.SelectedNode.Level == 2 && typenodes == deassemblyTreeView.SelectedNode)
                                || (deassemblyTreeView.SelectedNode.Level == 3 && typenodes == deassemblyTreeView.SelectedNode.Parent && deassemblyTreeView.SelectedNode.Index < methodindex)
                                || (deassemblyTreeView.SelectedNode.Level == 3 && typenodes != deassemblyTreeView.SelectedNode.Parent && namespacenodes.Index > deassemblyTreeView.SelectedNode.Parent.Parent.Index)
                                || (deassemblyTreeView.SelectedNode.Level == 3 && typenodes != deassemblyTreeView.SelectedNode.Parent && namespacenodes == deassemblyTreeView.SelectedNode.Parent.Parent && typenodes.Index > deassemblyTreeView.SelectedNode.Parent.Index)

                                ))
                                        {
                                            resetSearch = false;
                                            deassemblyTreeView.SelectedNode = typenodes.Nodes[methodindex + method.DeclaringType.NestedTypes.Count];
                                            goto escape;
                                        }
                                        ++incindex;
                                    }
                                }
                                ++methodindex;
                            }
                        }
                        catch { }
                    }
                }

            }

        escape: ;
        }

        private void searchListComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (searchListComboBox.SelectedIndex == 1)
            {
                searchTextBox.Text = "200000";
            }
            else if (searchListComboBox.SelectedIndex == 3)
            {
                searchTextBox.Text = "600000";
            }
        }

        private void searchTextBox_TextChanged(object sender, EventArgs e)
        {
            resetSearch = true;
        }

        private void searchTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!string.IsNullOrEmpty(searchTextBox.Text)
               && e.KeyChar == (char)Keys.Enter
               && deassemblyTreeView.Nodes.Count != 0)
            {
                search(searchTextBox.Text, searchListComboBox.SelectedIndex);
            }
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            string bytes = string.Empty;
            foreach (ListViewItem selectedItem in deassemblyListView.SelectedItems)
            {
                bytes += selectedItem.SubItems[5].Text;
            }
            Clipboard.SetText(bytes);
        }

        private void textToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string instructions = string.Empty;
            foreach (ListViewItem selectedItem in deassemblyListView.SelectedItems)
            {
                instructions += selectedItem.SubItems[3].Text + ' ' + selectedItem.SubItems[4].Text + '\r' + '\n';
            }
            Clipboard.SetText(instructions);
        }

        private void nopToolStripBtn_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in deassemblyListView.SelectedItems)
            {
                string s = new string('0', item.SubItems[5].Text.Length);
                item.SubItems[3].Text = "nop";
                item.SubItems[4].Text = "";
                item.SubItems[5].Text = s;
                write(uint.Parse(item.SubItems[1].Text, System.Globalization.NumberStyles.HexNumber), item.SubItems[5].Text);
            }

        }

        private void reverseBranchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in deassemblyListView.SelectedItems)
            {
                string bran = (item.SubItems[5].Text[0].ToString() + item.SubItems[5].Text[1].ToString()).ToLower();
                BranchReverser.Reverse(ref bran);
                write(uint.Parse(item.SubItems[1].Text, System.Globalization.NumberStyles.HexNumber), bran.ToUpper());
                item.SubItems[5].Text = item.SubItems[5].Text.Replace((item.SubItems[5].Text[0].ToString() + item.SubItems[5].Text[1].ToString()), bran.ToUpper());
            }
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshToolStripButton_Click(null, null);
        }

        private void gotoEntryPointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (assembly != null && assembly.ManifestModule.EntryPoint != null)
            {
                search(assembly.ManifestModule.EntryPoint.MDToken.ToInt32().ToString("x"), 3);
            }
        }

        private void gotoModuleConToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (assembly != null && assembly.ManifestModule.Types[0].Methods.Count != 0)
            {
                search("6000001", 3);
            }
        }

        private void dumpMethodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (deassemblyTreeView . SelectedNode == null) return;
                if (deassemblyTreeView.SelectedNode.ImageIndex != 2) return;
                string dump = string.Empty;

                MethodDef mtdtodump = (MethodDef)tokenResolver((int) deassemblyTreeView.SelectedNode.Tag);
                if (!mtdtodump.HasBody) return;
                int index = 0;
                foreach (Instruction inc in mtdtodump.Body.Instructions)
                {
                    dump += "/*" + deassemblyListView .Items[index++].SubItems[5].Text + "*/    " + inc.ToString() + '\r' + '\n';
                }
                SaveFileDialog ofd = new SaveFileDialog();
                ofd.FileName = mtdtodump.Name;
                ofd.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(ofd.FileName, dump);
                }
            }
            catch (Exception ex)
            {
                errorHandler.handleException(ex, assembly.Name.ToString());
                //Handle Exception//ErrorHandler.handleException(eee, asm.FullName);
            }
        }

        private void renameToolStripButton_Click(object sender, EventArgs e)
        {
            if (assembly == null) return;

            foreach (TypeDef t in assembly.ManifestModule.Types)
            {
                renameType(t);
            }

            TreeNode parentNode = deassemblyTreeView.SelectedNode;
            while (parentNode.Parent != null)
            {
                parentNode = parentNode.Parent;
            }

            deassemblyTreeView.AfterSelect -= new TreeViewEventHandler(deassemblyTreeView_AfterSelect);

            deassemblyTreeView.Nodes.Remove(parentNode);

            deassemblyTreeView.AfterSelect += new TreeViewEventHandler(deassemblyTreeView_AfterSelect);

            TreeNode assemblyNode = processAssembly(assembly);

            assemblyNode.Tag = assemblyLocation;

            deassemblyTreeView.Nodes.Insert(parentNode.Index,assemblyNode);

            deassemblyTreeView.SelectedNode = deassemblyTreeView.Nodes[parentNode.Index];
         
        }

        private void renameType(TypeDef type)
        {
            type.Name = "type" + (type.MDToken.Rid).ToString();

            try
            {
                foreach (TypeDef nestedType in type.NestedTypes)
                {
                    renameType(nestedType);
                }
            }
            catch { }
            try
            {
                foreach (MethodDef m in type.Methods)
                {
                    if (m.Name == ".ctor" || m.Name == ".cctor") continue;
                    m.Name = "method" + (m.MDToken.Rid ).ToString();
                }
            }
            catch { }
        }

        private void deassemblyListView_MouseDoubleClick(object sender, EventArgs e)
        {
            if (deassemblyListView.SelectedItems.Count != 0)
            {
                modifyForm mod = new
                modifyForm(deassemblyListView.SelectedItems[0].SubItems
                [5].Text,
                uint.Parse(deassemblyListView.SelectedItems[0].SubItems[1].Text, System.Globalization.NumberStyles.HexNumber) );
                mod.write = new modifyForm.writed(write);
                mod.refresh = new modifyForm.refreshd(refreshToolStripButton_Click);
                mod.Show();
            }
        }

        private void followJumpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Instruction jmp = null;
            MethodDef method = (MethodDef)tokenResolver((int)deassemblyTreeView.SelectedNode.Tag);
            jmp = method.Body.Instructions[deassemblyListView.SelectedItems[0].Index];
            
            if (jmp.Operand is Instruction)
            {
                //search();
                foreach (ListViewItem i in deassemblyListView.Items)
                {
                    if (i.Text == ((Instruction)(jmp.Operand)).Offset.ToString("x").ToUpper())
                    {
                        deassemblyListView.SelectedItems[0].Selected = false;
                        i.Selected = true;
                        i.EnsureVisible();
                        break;
                    }
                }
            }

        }

        private void deassemblyMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (deassemblyListView.SelectedItems.Count == 0) return;

            int length = deassemblyListView.SelectedItems[0].SubItems[5].Text.Length;
            traceToolStripMenuItem.Visible = followJumpToolStripMenuItem.Visible = false;
            if (length == 10)
            {
                traceToolStripMenuItem.Visible = true;
            }
            else if (length == 4)
            {
                followJumpToolStripMenuItem.Visible = true;
            }
        }

    }
}

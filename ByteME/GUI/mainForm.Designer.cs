﻿namespace ByteME.GUI
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.closeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.refreshToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.opCodeHighlightingToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.deassemblyTreeView = new System.Windows.Forms.TreeView();
            this.deassemblyNamesMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gotoEntryPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gotoModuleConToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dumpMethodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripButton = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.deassemblyListView = new System.Windows.Forms.ListView();
            this.incNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.incVA = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.incRVA = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.incOPCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.incOperand = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.incBytes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.deassemblyMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.followJumpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.traceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.nopToolStripBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyToolStripBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.reverseBranchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoListView = new System.Windows.Forms.ListView();
            this.nameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.valColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.retTypeLabel = new System.Windows.Forms.Label();
            this.tokenLabel = new System.Windows.Forms.Label();
            this.rvaLabel = new System.Windows.Forms.Label();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.searchListComboBox = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.deassemblyNamesMenuStrip.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.deassemblyMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripButton,
            this.saveToolStripButton,
            this.closeToolStripButton,
            this.refreshToolStripButton,
            this.opCodeHighlightingToolStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(950, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = global::ByteME.Properties.Resources.Open;
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Open";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = global::ByteME.Properties.Resources.Save;
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // closeToolStripButton
            // 
            this.closeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.closeToolStripButton.Image = global::ByteME.Properties.Resources.Close;
            this.closeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.closeToolStripButton.Name = "closeToolStripButton";
            this.closeToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.closeToolStripButton.Text = "Close";
            this.closeToolStripButton.Click += new System.EventHandler(this.closeToolStripButton_Click);
            // 
            // refreshToolStripButton
            // 
            this.refreshToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshToolStripButton.Image = global::ByteME.Properties.Resources.Refresh;
            this.refreshToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshToolStripButton.Name = "refreshToolStripButton";
            this.refreshToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.refreshToolStripButton.Text = "Refresh";
            this.refreshToolStripButton.Click += new System.EventHandler(this.refreshToolStripButton_Click);
            // 
            // opCodeHighlightingToolStripButton
            // 
            this.opCodeHighlightingToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.opCodeHighlightingToolStripButton.Image = global::ByteME.Properties.Resources.HighLighter;
            this.opCodeHighlightingToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.opCodeHighlightingToolStripButton.Name = "opCodeHighlightingToolStripButton";
            this.opCodeHighlightingToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.opCodeHighlightingToolStripButton.Text = "OpCode highlighting";
            this.opCodeHighlightingToolStripButton.Click += new System.EventHandler(this.opCodeHighlightingToolStripButton_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.deassemblyTreeView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(950, 468);
            this.splitContainer1.SplitterDistance = 222;
            this.splitContainer1.TabIndex = 3;
            // 
            // deassemblyTreeView
            // 
            this.deassemblyTreeView.AllowDrop = true;
            this.deassemblyTreeView.ContextMenuStrip = this.deassemblyNamesMenuStrip;
            this.deassemblyTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deassemblyTreeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deassemblyTreeView.HideSelection = false;
            this.deassemblyTreeView.ImageIndex = 0;
            this.deassemblyTreeView.ImageList = this.imageList1;
            this.deassemblyTreeView.Location = new System.Drawing.Point(0, 0);
            this.deassemblyTreeView.Name = "deassemblyTreeView";
            this.deassemblyTreeView.SelectedImageIndex = 0;
            this.deassemblyTreeView.Size = new System.Drawing.Size(222, 468);
            this.deassemblyTreeView.TabIndex = 0;
            this.deassemblyTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.deassemblyTreeView_AfterSelect);
            this.deassemblyTreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.deassemblyTreeView_DragDrop);
            this.deassemblyTreeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.deassemblyTreeView_DragEnter);
            // 
            // deassemblyNamesMenuStrip
            // 
            this.deassemblyNamesMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gotoEntryPointToolStripMenuItem,
            this.gotoModuleConToolStripMenuItem,
            this.dumpMethodToolStripMenuItem,
            this.renameToolStripButton});
            this.deassemblyNamesMenuStrip.Name = "contextMenuStrip1";
            this.deassemblyNamesMenuStrip.Size = new System.Drawing.Size(211, 92);
            // 
            // gotoEntryPointToolStripMenuItem
            // 
            this.gotoEntryPointToolStripMenuItem.Name = "gotoEntryPointToolStripMenuItem";
            this.gotoEntryPointToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.gotoEntryPointToolStripMenuItem.Text = "Goto EntryPoint";
            this.gotoEntryPointToolStripMenuItem.Click += new System.EventHandler(this.gotoEntryPointToolStripMenuItem_Click);
            // 
            // gotoModuleConToolStripMenuItem
            // 
            this.gotoModuleConToolStripMenuItem.Name = "gotoModuleConToolStripMenuItem";
            this.gotoModuleConToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.gotoModuleConToolStripMenuItem.Text = "Goto Module Constructor";
            this.gotoModuleConToolStripMenuItem.Click += new System.EventHandler(this.gotoModuleConToolStripMenuItem_Click);
            // 
            // dumpMethodToolStripMenuItem
            // 
            this.dumpMethodToolStripMenuItem.Name = "dumpMethodToolStripMenuItem";
            this.dumpMethodToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.dumpMethodToolStripMenuItem.Text = "Dump method";
            this.dumpMethodToolStripMenuItem.Click += new System.EventHandler(this.dumpMethodToolStripMenuItem_Click);
            // 
            // renameToolStripButton
            // 
            this.renameToolStripButton.Name = "renameToolStripButton";
            this.renameToolStripButton.Size = new System.Drawing.Size(210, 22);
            this.renameToolStripButton.Text = "Rename methods";
            this.renameToolStripButton.Click += new System.EventHandler(this.renameToolStripButton_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "imageList1.ImageStream6.bmp");
            this.imageList1.Images.SetKeyName(1, "imageList1.ImageStream1.bmp");
            this.imageList1.Images.SetKeyName(2, "imageList1.ImageStream2.bmp");
            this.imageList1.Images.SetKeyName(3, "imageList1.ImageStream3.bmp");
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.deassemblyListView);
            this.splitContainer2.Panel1.Controls.Add(this.infoListView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.retTypeLabel);
            this.splitContainer2.Panel2.Controls.Add(this.tokenLabel);
            this.splitContainer2.Panel2.Controls.Add(this.rvaLabel);
            this.splitContainer2.Panel2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.splitContainer2.Size = new System.Drawing.Size(724, 468);
            this.splitContainer2.SplitterDistance = 436;
            this.splitContainer2.TabIndex = 0;
            // 
            // deassemblyListView
            // 
            this.deassemblyListView.AllowColumnReorder = true;
            this.deassemblyListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.incNo,
            this.incVA,
            this.incRVA,
            this.incOPCode,
            this.incOperand,
            this.incBytes});
            this.deassemblyListView.ContextMenuStrip = this.deassemblyMenuStrip;
            this.deassemblyListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deassemblyListView.FullRowSelect = true;
            this.deassemblyListView.HideSelection = false;
            this.deassemblyListView.Location = new System.Drawing.Point(0, 0);
            this.deassemblyListView.Name = "deassemblyListView";
            this.deassemblyListView.Size = new System.Drawing.Size(724, 436);
            this.deassemblyListView.TabIndex = 2;
            this.deassemblyListView.UseCompatibleStateImageBehavior = false;
            this.deassemblyListView.View = System.Windows.Forms.View.Details;
            this.deassemblyListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.deassemblyListView_MouseDoubleClick);
            // 
            // incNo
            // 
            this.incNo.Text = "#";
            this.incNo.Width = 31;
            // 
            // incVA
            // 
            this.incVA.Text = "VA";
            this.incVA.Width = 48;
            // 
            // incRVA
            // 
            this.incRVA.Text = "RVA";
            this.incRVA.Width = 54;
            // 
            // incOPCode
            // 
            this.incOPCode.Text = "OP Code";
            this.incOPCode.Width = 80;
            // 
            // incOperand
            // 
            this.incOperand.Text = "Operand";
            this.incOperand.Width = 378;
            // 
            // incBytes
            // 
            this.incBytes.Text = "Byte";
            this.incBytes.Width = 102;
            // 
            // deassemblyMenuStrip
            // 
            this.deassemblyMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.followJumpToolStripMenuItem,
            this.traceToolStripMenuItem,
            this.copyToolStripButton,
            this.toolStripSeparator2,
            this.nopToolStripBtn,
            this.modifyToolStripBtn,
            this.reverseBranchToolStripMenuItem,
            this.toolStripSeparator1,
            this.refreshToolStripMenuItem});
            this.deassemblyMenuStrip.Name = "contextMenuStrip1";
            this.deassemblyMenuStrip.Size = new System.Drawing.Size(155, 192);
            this.deassemblyMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.deassemblyMenuStrip_Opening);
            // 
            // followJumpToolStripMenuItem
            // 
            this.followJumpToolStripMenuItem.Name = "followJumpToolStripMenuItem";
            this.followJumpToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.followJumpToolStripMenuItem.Text = "Follow jump";
            this.followJumpToolStripMenuItem.Click += new System.EventHandler(this.followJumpToolStripMenuItem_Click);
            // 
            // traceToolStripMenuItem
            // 
            this.traceToolStripMenuItem.Name = "traceToolStripMenuItem";
            this.traceToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.traceToolStripMenuItem.Text = "Trace";
            this.traceToolStripMenuItem.Click += new System.EventHandler(this.traceToolStripMenuItem_Click);
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bytesToolStripMenuItem,
            this.textToolStripMenuItem});
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(154, 22);
            this.copyToolStripButton.Text = "Copy";
            this.copyToolStripButton.Click += new System.EventHandler(this.copyToolStripButton_Click);
            // 
            // bytesToolStripMenuItem
            // 
            this.bytesToolStripMenuItem.Name = "bytesToolStripMenuItem";
            this.bytesToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.bytesToolStripMenuItem.Text = "Bytes";
            this.bytesToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripButton_Click);
            // 
            // textToolStripMenuItem
            // 
            this.textToolStripMenuItem.Name = "textToolStripMenuItem";
            this.textToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.textToolStripMenuItem.Text = "Instructions";
            this.textToolStripMenuItem.Click += new System.EventHandler(this.textToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(151, 6);
            // 
            // nopToolStripBtn
            // 
            this.nopToolStripBtn.Name = "nopToolStripBtn";
            this.nopToolStripBtn.Size = new System.Drawing.Size(154, 22);
            this.nopToolStripBtn.Text = "NOP";
            this.nopToolStripBtn.Click += new System.EventHandler(this.nopToolStripBtn_Click);
            // 
            // modifyToolStripBtn
            // 
            this.modifyToolStripBtn.Name = "modifyToolStripBtn";
            this.modifyToolStripBtn.Size = new System.Drawing.Size(154, 22);
            this.modifyToolStripBtn.Text = "Modify";
            this.modifyToolStripBtn.Click += new System.EventHandler(this.deassemblyListView_MouseDoubleClick);
            // 
            // reverseBranchToolStripMenuItem
            // 
            this.reverseBranchToolStripMenuItem.Name = "reverseBranchToolStripMenuItem";
            this.reverseBranchToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.reverseBranchToolStripMenuItem.Text = "Reverse branch";
            this.reverseBranchToolStripMenuItem.Click += new System.EventHandler(this.reverseBranchToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(151, 6);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // infoListView
            // 
            this.infoListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameColumn,
            this.valColumn});
            this.infoListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoListView.FullRowSelect = true;
            this.infoListView.Location = this.deassemblyListView.Location;
            this.infoListView.Name = "infoListView";
            this.infoListView.Size = this.deassemblyListView.Size;
            this.infoListView.TabIndex = 0;
            this.infoListView.UseCompatibleStateImageBehavior = false;
            this.infoListView.View = System.Windows.Forms.View.Details;
            // 
            // nameColumn
            // 
            this.nameColumn.Text = "Name";
            this.nameColumn.Width = 200;
            // 
            // valColumn
            // 
            this.valColumn.Text = "Value";
            this.valColumn.Width = 480;
            // 
            // retTypeLabel
            // 
            this.retTypeLabel.AutoSize = true;
            this.retTypeLabel.Location = new System.Drawing.Point(253, 4);
            this.retTypeLabel.Name = "retTypeLabel";
            this.retTypeLabel.Size = new System.Drawing.Size(0, 13);
            this.retTypeLabel.TabIndex = 2;
            // 
            // tokenLabel
            // 
            this.tokenLabel.AutoSize = true;
            this.tokenLabel.Location = new System.Drawing.Point(124, 4);
            this.tokenLabel.Name = "tokenLabel";
            this.tokenLabel.Size = new System.Drawing.Size(0, 13);
            this.tokenLabel.TabIndex = 1;
            // 
            // rvaLabel
            // 
            this.rvaLabel.AutoSize = true;
            this.rvaLabel.Location = new System.Drawing.Point(8, 4);
            this.rvaLabel.Name = "rvaLabel";
            this.rvaLabel.Size = new System.Drawing.Size(0, 13);
            this.rvaLabel.TabIndex = 0;
            // 
            // searchTextBox
            // 
            this.searchTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchTextBox.Location = new System.Drawing.Point(722, 1);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(135, 20);
            this.searchTextBox.TabIndex = 8;
            this.searchTextBox.TextChanged += new System.EventHandler(this.searchTextBox_TextChanged);
            this.searchTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.searchTextBox_KeyPress);
            // 
            // searchListComboBox
            // 
            this.searchListComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.searchListComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.searchListComboBox.FormattingEnabled = true;
            this.searchListComboBox.Items.AddRange(new object[] {
            "Type name",
            "Type token",
            "Method name",
            "Method token",
            "Method RVA",
            "Operand",
            "String",
            "OPCode RVA"});
            this.searchListComboBox.Location = new System.Drawing.Point(859, 1);
            this.searchListComboBox.Name = "searchListComboBox";
            this.searchListComboBox.Size = new System.Drawing.Size(89, 21);
            this.searchListComboBox.TabIndex = 7;
            this.searchListComboBox.SelectedIndexChanged += new System.EventHandler(this.searchListComboBox_SelectedIndexChanged);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 493);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.searchListComboBox);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "mainForm";
            this.Text = "ByteME";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.deassemblyNamesMenuStrip.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            this.deassemblyMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton closeToolStripButton;
        private System.Windows.Forms.ToolStripButton refreshToolStripButton;
        private System.Windows.Forms.ToolStripButton opCodeHighlightingToolStripButton;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView deassemblyTreeView;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListView deassemblyListView;
        private System.Windows.Forms.ColumnHeader incNo;
        private System.Windows.Forms.ColumnHeader incVA;
        private System.Windows.Forms.ColumnHeader incRVA;
        private System.Windows.Forms.ColumnHeader incOPCode;
        private System.Windows.Forms.ColumnHeader incOperand;
        private System.Windows.Forms.ColumnHeader incBytes;
        private System.Windows.Forms.ListView infoListView;
        private System.Windows.Forms.ColumnHeader nameColumn;
        private System.Windows.Forms.ColumnHeader valColumn;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label retTypeLabel;
        private System.Windows.Forms.Label tokenLabel;
        private System.Windows.Forms.Label rvaLabel;
        private System.Windows.Forms.ContextMenuStrip deassemblyMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem traceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem bytesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem nopToolStripBtn;
        private System.Windows.Forms.ToolStripMenuItem reverseBranchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyToolStripBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.ComboBox searchListComboBox;
        private System.Windows.Forms.ContextMenuStrip deassemblyNamesMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem gotoEntryPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gotoModuleConToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dumpMethodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripButton;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem followJumpToolStripMenuItem;

    }
}
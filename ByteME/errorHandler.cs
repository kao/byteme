﻿using System;
using System.Collections.Generic;
using System.Text;
using ByteME.GUI;
namespace ByteME
{
    class errorHandler
    {

        static bool IgnoreErrors;

        public static void handleException(Exception ex, string asmname)
        {
            if (!IgnoreErrors)
            {
                errorHandlerForm err = new errorHandlerForm(asmname, ex.Message, ex.StackTrace);
                err.ShowDialog();
                IgnoreErrors = err.ignoreAllErrorsCheckBox.Checked;
            }
        }

        public static void handleExceptions(object o, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exception exc = (Exception)e.ExceptionObject;
                errorHandlerForm er = new errorHandlerForm("no name", exc.Message, exc.StackTrace);
                er.ShowDialog();
            }
            catch { }
        }
        public static void threadExceptions(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Exception exc = (Exception)e.Exception;
            errorHandlerForm er = new errorHandlerForm("no name", exc.Message, exc.StackTrace);
            er.ShowDialog();
        }
    }
}

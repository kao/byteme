﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Text.RegularExpressions;
using dnlib.DotNet.Emit;

namespace ByteME
{
    public static class IL_Helper
    {

        public static byte[] toByteArray(string value)
        {
            byte[] bytes = null;
            int string_length = value.Length;
            int character_index = (value.StartsWith("0x", StringComparison.Ordinal)) ? 2 : 0; // Does the string define leading HEX indicator '0x'. Adjust starting index accordingly.               
            int number_of_characters = string_length - character_index;
            bool add_leading_zero = false;
            if (0 != (number_of_characters % 2))
            {
                add_leading_zero = true;

                number_of_characters += 1;  // Leading '0' has been striped from the string presentation.
            }
            bytes = new byte[number_of_characters / 2]; // Initialize our byte array to hold the converted string.
            int write_index = 0;
            if (add_leading_zero)
            {
                bytes[write_index++] = cToByte(value[character_index], character_index);
                character_index += 1;
            }
            for (int read_index = character_index; read_index < value.Length; read_index += 2)
            {
                byte upper = cToByte(value[read_index], read_index, 4);
                byte lower = cToByte(value[read_index + 1], read_index + 1);
                bytes[write_index++] = (byte)(upper | lower);
            }
            return bytes;
        }
      
        static byte cToByte(char character, int index, int shift = 0)
        {
            byte value = (byte)character;
            if (((0x40 < value) && (0x47 > value)) || ((0x60 < value) && (0x67 > value)))
            {
                if (0x40 == (0x40 & value))
                {
                    if (0x20 == (0x20 & value))
                        value = (byte)(((value + 0xA) - 0x61) << shift);
                    else
                        value = (byte)(((value + 0xA) - 0x41) << shift);
                }
            }
            else if ((0x29 < value) && (0x40 > value))
                value = (byte)((value - 0x30) << shift);
            else
                throw new InvalidOperationException(String.Format("Character '{0}' at index '{1}' is not valid alphanumeric character.", character, index));

            return value;
        }

        public static string ReverseToken(string token)
        {
            //token = token.Insert(8, "0");
            string rewrittenrevbytes = string.Empty;
            for (int i = token.Length - 1; i >= 0; --i)
            {
                rewrittenrevbytes += token[i];
            }
            rewrittenrevbytes = rewrittenrevbytes.Insert(2, "|").Insert(5, "|").Insert(8, "|")
        ;
            string rewrittenbytes = string.Empty;
            foreach (string hexa in rewrittenrevbytes.Split('|'))
            {
                for (int i = hexa.Length - 1; i >= 0; --i)
                {
                    rewrittenbytes += hexa[i];
                }
            }

            return rewrittenbytes;
        }

        private static OpCode[] _table = new OpCode[] { 
        OpCodes.Add, OpCodes.Add_Ovf, OpCodes.Add_Ovf_Un, OpCodes.And, OpCodes.Arglist, OpCodes.Beq, OpCodes.Beq_S, OpCodes.Bge, OpCodes.Bge_S, OpCodes.Bge_Un, OpCodes.Bge_Un_S, OpCodes.Bgt, OpCodes.Bgt_S, OpCodes.Bgt_Un, OpCodes.Bgt_Un_S, OpCodes.Ble, 
        OpCodes.Ble_S, OpCodes.Ble_Un, OpCodes.Ble_Un_S, OpCodes.Blt, OpCodes.Blt_S, OpCodes.Blt_Un, OpCodes.Blt_Un_S, OpCodes.Bne_Un, OpCodes.Bne_Un_S, OpCodes.Box, OpCodes.Br, OpCodes.Br_S, OpCodes.Break, OpCodes.Brfalse, OpCodes.Brfalse_S, OpCodes.Brtrue, 
        OpCodes.Brtrue_S, OpCodes.Call, OpCodes.Calli, OpCodes.Callvirt, OpCodes.Castclass, OpCodes.Ceq, OpCodes.Cgt, OpCodes.Cgt_Un, OpCodes.Ckfinite, OpCodes.Clt, OpCodes.Clt_Un, OpCodes.Constrained, OpCodes.Conv_I, OpCodes.Conv_I1, OpCodes.Conv_I2, OpCodes.Conv_I4, 
        OpCodes.Conv_I8, OpCodes.Conv_Ovf_I, OpCodes.Conv_Ovf_I_Un, OpCodes.Conv_Ovf_I1, OpCodes.Conv_Ovf_I1_Un, OpCodes.Conv_Ovf_I2, OpCodes.Conv_Ovf_I2_Un, OpCodes.Conv_Ovf_I4, OpCodes.Conv_Ovf_I4_Un, OpCodes.Conv_Ovf_I8, OpCodes.Conv_Ovf_I8_Un, OpCodes.Conv_Ovf_U, OpCodes.Conv_Ovf_U_Un, OpCodes.Conv_Ovf_U1, OpCodes.Conv_Ovf_U1_Un, OpCodes.Conv_Ovf_U2, 
        OpCodes.Conv_Ovf_U2_Un, OpCodes.Conv_Ovf_U4, OpCodes.Conv_Ovf_U4_Un, OpCodes.Conv_Ovf_U8, OpCodes.Conv_Ovf_U8_Un, OpCodes.Conv_R_Un, OpCodes.Conv_R4, OpCodes.Conv_R8, OpCodes.Conv_U, OpCodes.Conv_U1, OpCodes.Conv_U2, OpCodes.Conv_U4, OpCodes.Conv_U8, OpCodes.Cpblk, OpCodes.Cpobj, OpCodes.Div, 
        OpCodes.Div_Un, OpCodes.Dup, OpCodes.Endfilter, OpCodes.Endfinally, OpCodes.Initblk, OpCodes.Initobj, OpCodes.Isinst, OpCodes.Jmp, OpCodes.Ldarg, OpCodes.Ldarg_0, OpCodes.Ldarg_1, OpCodes.Ldarg_2, OpCodes.Ldarg_3, OpCodes.Ldarg_S, OpCodes.Ldarga, OpCodes.Ldarga_S, 
        OpCodes.Ldc_I4, OpCodes.Ldc_I4_0, OpCodes.Ldc_I4_1, OpCodes.Ldc_I4_2, OpCodes.Ldc_I4_3, OpCodes.Ldc_I4_4, OpCodes.Ldc_I4_5, OpCodes.Ldc_I4_6, OpCodes.Ldc_I4_7, OpCodes.Ldc_I4_8, OpCodes.Ldc_I4_M1, OpCodes.Ldc_I4_S, OpCodes.Ldc_I8, OpCodes.Ldc_R4, OpCodes.Ldc_R8, OpCodes.Ldlen, 
        OpCodes.Ldelem_I, OpCodes.Ldelem_I1, OpCodes.Ldelem_I2, OpCodes.Ldelem_I4, OpCodes.Ldelem_I8, OpCodes.Ldelem_R4, OpCodes.Ldelem_R8, OpCodes.Ldelem_Ref, OpCodes.Ldelem_U1, OpCodes.Ldelem_U2, OpCodes.Ldelem_U4, OpCodes.Ldelema, OpCodes.Ldfld, OpCodes.Ldflda, OpCodes.Ldftn, OpCodes.Ldind_I, 
        OpCodes.Ldind_I1, OpCodes.Ldind_I2, OpCodes.Ldind_I4, OpCodes.Ldind_I8, OpCodes.Ldind_R4, OpCodes.Ldind_R8, OpCodes.Ldind_Ref, OpCodes.Ldind_U1, OpCodes.Ldind_U2, OpCodes.Ldind_U4, OpCodes.Ldlen, OpCodes.Ldloc, OpCodes.Ldloc_0, OpCodes.Ldloc_1, OpCodes.Ldloc_2, OpCodes.Ldloc_3, 
        OpCodes.Ldloc_S, OpCodes.Ldloca, OpCodes.Ldloca_S, OpCodes.Ldnull, OpCodes.Ldobj, OpCodes.Ldsfld, OpCodes.Ldsflda, OpCodes.Ldstr, OpCodes.Ldtoken, OpCodes.Ldvirtftn, OpCodes.Leave, OpCodes.Leave_S, OpCodes.Localloc, OpCodes.Mkrefany, OpCodes.Mul, OpCodes.Mul_Ovf, 
        OpCodes.Mul_Ovf_Un, OpCodes.Neg, OpCodes.Newarr, OpCodes.Newobj, OpCodes.Nop, OpCodes.Not, OpCodes.Or, OpCodes.Pop,
        OpCodes.Readonly, OpCodes.Refanytype, OpCodes.Refanyval, OpCodes.Rem, OpCodes.Rem_Un, OpCodes.Ret, OpCodes.Rethrow, OpCodes.Shl, OpCodes.Shr, OpCodes.Shr_Un, OpCodes.Sizeof, OpCodes.Starg, OpCodes.Starg_S, OpCodes.Stelem, OpCodes.Stelem_I, OpCodes.Stelem_I1, 
        OpCodes.Stelem_I2, OpCodes.Stelem_I4, OpCodes.Stelem_I8, OpCodes.Stelem_R4, OpCodes.Stelem_R8, OpCodes.Stelem_Ref, OpCodes.Stfld, OpCodes.Stind_I, OpCodes.Stind_I1, OpCodes.Stind_I2, OpCodes.Stind_I4, OpCodes.Stind_I8, OpCodes.Stind_R4, OpCodes.Stind_R8, OpCodes.Stind_Ref, OpCodes.Stloc, 
        OpCodes.Stloc_0, OpCodes.Stloc_1, OpCodes.Stloc_2, OpCodes.Stloc_3, OpCodes.Stloc_S, OpCodes.Stobj, OpCodes.Stsfld, OpCodes.Sub, OpCodes.Sub_Ovf, OpCodes.Sub_Ovf_Un, OpCodes.Switch, OpCodes.Tailcall, OpCodes.Throw, OpCodes.Unaligned, OpCodes.Unbox, OpCodes.Unbox_Any, 
        OpCodes.Volatile, OpCodes.Xor
     };


        public static string Hex2IL(string hex)
        {
            for (int i = 0; i < _table.Length; i++)
            {
                OpCode code = _table[i];
                string codehex = string.Format("{0:X2}", code.Value);
                if (codehex == hex)
                {
                    hex = code.Name.ToLower().Replace("_", "."); 
                }
            }

            return hex;
        }

        public static string IL2Hex(string IL)
        {
            for (int i = 0; i < _table.Length; ++i)
            {
                OpCode code = _table[i];
                if (code.Name.ToLower().Replace("_",".") == IL.ToLower())
                {
                    IL = string.Format("{0:X2}", code.Value);
                }
            }

            return IL;
        }

    }
}
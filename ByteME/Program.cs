﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ByteME
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(errorHandler.threadExceptions);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(errorHandler.handleExceptions);
            Application.Run(new GUI.mainForm(args));
        }
    }
}
